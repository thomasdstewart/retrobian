module gitlab.com/thomasdstewart/retrobian

go 1.22

toolchain go1.23.4

require github.com/theNewDynamic/gohugo-theme-ananke/v2 v2.11.2 // indirect
