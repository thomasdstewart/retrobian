---
title: "About"
description: "Retro Debian Installations"
featured_image: "/install/0.93-debian-login.png"
---
Welcome,

I first used SuSE Linux 5.2 in 1998 thanks to my Dad who got a copy of it it on a computer magazine. At the time I had only ever user used DOS and Windows. He printed the entire manual and persevered with the installation process it get it working on the families home computer, a Gateway 2000 Pentium 90. Running an entirely different 'thing' on a computer and seeing the different XFree86 user interface fascinated me. After this I was hooked on Linux! I discovered Debian sometime in 2000 when Debian 2.2 Potato was the current stable release. For a few years I rattled around a number of other distributions: Red Hat, SuSE, LFS (Linux From Scratch), Sorcerer GNU/Linux, SuSE, Debian. I permanently switched to Debian as my main driver sometime in 2002 after a friend said it was not as hard as I thought. Debian is awesome and it's quite amazing to see the progress Linux has been made over the years.

I'm not sure why, but I have an interest in older Debian Installations and this site enables anyone see and try out older versions of Debian and documents how to do this. I have published this because I have over the years done this several times with many different versions in an ad-hoc manor. There is not a lot of point in this, apart from: why not. I think it comes from a nostalgia for the olden days.

Thanks

Thomas Stewart

thomas@stewarts.org.uk
