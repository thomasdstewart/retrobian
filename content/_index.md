---
title: "Retrobian"
description: "Retro Debian Installations"
featured_image: "/install/0.93-debian-login.png"
---
This site documents how to use older versions of Debian providing installation information. It also shows how to use existing installations using disk images hosted online allowing anyone to easily try without having to install. Below is the list of versions available.

| Debian Version                          | Code Name                                         | Release Date  | Repo Size | Linux Version | GCC Version |
| ---                                     | ---                                               | ---           | ---       | ---           | ---         |
| [0.91]({{< relref "0.91-debian.md" >}}) |                                                   | January 1994  | 84M       | 0.99.14w      | 2.5.7       |
| [0.93]({{< relref "0.93-debian.md" >}}) |                                                   | November 1995 | 196M      | 1.2.13        | 2.6.3       |
| [1.1]({{<  relref "1.1-buzz.md" >}})    | Buzz                                              | June 1996     | 478M      | 2.0.0         | 2.7.2       |
| [1.2]({{<  relref "1.2-rex.md" >}})     | Rex                                               | December 1996 | 587M      | 2.0.27        | 2.7.2.1     |
| [1.3]({{<  relref "1.3-bo.md" >}})      | Bo                                                | June 1997     | 1.1G      | 2.0.29        | 2.7.2.1     |
| [2.0]({{<  relref "2.0-hamm.md" >}})    | [Hamm](https://www.debian.org/releases/hamm/)     | July 1998     | 2.0G      | 2.0.34        | 2.7.2.3     |
| [2.1]({{<  relref "2.1-slink.md" >}})   | [Slink](https://www.debian.org/releases/slink/)   | March 1999    | 4.2G      | 2.0.38        | 2.7.2.3     |
| [2.2]({{<  relref "2.2-potato.md" >}})  | [Potato](https://www.debian.org/releases/potato/) | August 2000   | 6.8G      | 2.2.19        | 2.95.2      |
| [3.0]({{<  relref "3.0-woody.md" >}})   | [Woody](https://www.debian.org/releases/woody/)   | July 2002     | 11G       | 2.2.20        | 2.95.4      |

[Debian version history on Wikipedia](https://en.wikipedia.org/wiki/Debian_version_history)

[A Brief History of Debian - Chapter 4. A Detailed History - The 0.x Releases](https://www.debian.org/doc/manuals/project-history/detailed.en.html#rel-0)
